import llvmlite.binding as llvm
from llvmlite import ir
from ExprParser import ExprParser
from ExprLexer import ExprLexer
from ExprVisitor import ExprVisitor
from antlr4 import CommonTokenStream, FileStream
from ctypes import CFUNCTYPE, c_int
import ctypes
from graphviz import Digraph
import pprint

class VisitorCompiler(ExprVisitor):
    def __init__(self):
        self.module = ir.Module(name="turing_machine")
        self.builder = None
        self.function = None
        self.tape = None
        self.head = None
        self.states = {}
        self.input_array = None
        self.blank_symbol = None
        self.start_state = None
        self.length = None
        self.graph = Digraph(comment="turing_machine")
        self.optimized_module = None

    def visitMachine(self, ctx: ExprParser.MachineContext):
        # Create main function
        func_type = ir.FunctionType(ir.IntType(32), [])
        self.function = ir.Function(self.module, func_type, name="main")
        block = self.function.append_basic_block(name="entry")
        self.builder = ir.IRBuilder(block)

        # Visit input, blank, start, and table statements
        self.visitChildren(ctx)

        # Initialize tape and head
        self.length = len(ctx.inputstat().vallist().int_()) + 2
        self.tape = self.builder.alloca(ir.ArrayType(ir.IntType(32), self.length), name="tape")
        self.head = self.builder.alloca(ir.IntType(32), name="head")
        self.builder.store(ir.Constant(ir.IntType(32), 1), self.head)
        blank_val = ord(self.blank_symbol)

        # Copy input to tape with blank spaces
        ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), 0)])
        self.builder.store(ir.Constant(ir.IntType(32), blank_val), ptr)
        for i, val in enumerate(self.input_array):
            ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), i + 1)])
            self.builder.store(ir.Constant(ir.IntType(32), val), ptr)
        ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), self.length - 1)])
        self.builder.store(ir.Constant(ir.IntType(32), blank_val), ptr)

        # Start state machine
        self.build_graph()
        self.build_state_machine()
        return self.module

    def visitInputstat(self, ctx: ExprParser.InputstatContext):
        self.input_array = eval(ctx.vallist().getText())

    def visitBlanckstat(self, ctx: ExprParser.BlanckstatContext):
        self.blank_symbol = eval(ctx.strlit().getText())

    def visitStartstat(self, ctx: ExprParser.StartstatContext):
        self.start_state = ctx.id_().getText()

    def visitStrlit(self, ctx: ExprParser.StrlitContext):
        return ctx.getText()[1:-1]

    def visitTablestat(self, ctx: ExprParser.TablestatContext):
        for state in ctx.defstate():
            state_name = state.id_().getText()
            self.states[state_name] = {}
            for rule in state.defrule():
                actions = rule.defactions().defaction()
                if isinstance(rule.vallist(), ExprParser.VallistContext):
                    conditions = eval(rule.getText().split(':')[0])
                    for condition in conditions:
                        self.states[state_name][condition] = actions
                else:
                    if isinstance(rule.int_(), ExprParser.IntContext):
                        symbol = int(rule.int_().getText())
                    else:
                        symbol = self.visit(rule.strlit())
                    self.states[state_name][symbol] = actions

    def build_graph(self):
        pprint.pprint(self.states)
        self.graph.node("done")
        for i, state in enumerate(self.states):
            self.graph.node(state, state)
            for j, symbol in enumerate(self.states[state]):
                for action in self.states[state][symbol]:
                    action = action.getChild(0)
                    if isinstance(action, ExprParser.DefmoveContext):
                        lat_move = action.getChild(0).getText()
                        ssymbol = str(symbol) if not isinstance(symbol, str) else symbol
                        condition = str(symbol) if ssymbol != self.blank_symbol else "blank"
                        if len(self.states[state][symbol]) == 1:
                            if action.getChildCount() == 3:
                                goto = action.getChild(2).getText()
                                self.graph.edge(state, goto, label=condition + ":" + lat_move)
                            else:
                                self.graph.edge(state, state, label=condition + ":" + lat_move)
                        else:
                            write = ""
                            for act in self.states[state][symbol]:
                                act = act.getChild(0)
                                if isinstance(act, ExprParser.DefwriteContext):
                                    write = act.getChild(2).getText()
                            if action.getChildCount() == 3:
                                goto = action.getChild(2).getText()
                                self.graph.edge(state, goto, label=condition + ":" + lat_move + "&write" + write)
                            else:
                                self.graph.edge(state, state, label=condition + ":" + lat_move + "&write" + write)
        self.graph.save("machine.dot")

    def build_state_machine(self):
        state_blocks = {}

        for i, state in enumerate(self.states):
            block = self.function.append_basic_block(f"state_{state}")
            state_blocks[state] = block

        done_block = self.function.append_basic_block('done')
        state_blocks['done'] = done_block
        self.builder.branch(state_blocks[self.start_state])
        self.builder.position_at_end(state_blocks['done'])
        self.builder.ret(ir.Constant(ir.IntType(32), 0))

        for i, (state, rules) in enumerate(self.states.items()):
            self.builder.position_at_end(state_blocks[state])

            # Read current symbol
            head_val = self.builder.load(self.head)
            ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), head_val])
            current_symbol = self.builder.load(ptr)

            # Create switch for rules
            rule_switch = self.builder.switch(current_symbol, state_blocks[state])

            for symbol, actions in rules.items():
                symbol_block = self.function.append_basic_block(f"state_{state}_symbol_{symbol}")

                rule_switch.add_case(ir.Constant(ir.IntType(32), ord(symbol) if isinstance(symbol, str) else symbol), symbol_block)
                self.builder.position_at_end(symbol_block)

                for action in actions:
                    action = action.getChild(0)
                    if isinstance(action, ExprParser.DefwriteContext):
                        write_val = int(action.int_().getText())
                        self.builder.store(ir.Constant(ir.IntType(32), write_val), ptr)
                    elif isinstance(action, ExprParser.DefmoveContext):
                        direction = action.getText()[0]
                        # move head
                        if direction == 'L':
                            self.builder.store(self.builder.sub(head_val, ir.Constant(ir.IntType(32), 1)), self.head)
                        elif direction == 'R':
                            self.builder.store(self.builder.add(head_val, ir.Constant(ir.IntType(32), 1)), self.head)
                        # move state
                        if action.getChildCount() == 1:
                            self.builder.branch(state_blocks[state])
                        elif action.id_():
                            self.builder.branch(state_blocks[action.id_().getText()])
                        elif action.getChild(2).getText() == 'done':
                            self.builder.branch(state_blocks['done'])

    def optimize_module(self):
        llvm.initialize()
        llvm.initialize_native_target()
        llvm.initialize_native_asmprinter()

        print("Before Optimization:\n", str(self.module))
        
        pmb = llvm.create_pass_manager_builder()
        pmb.opt_level = 2
        pm = llvm.create_module_pass_manager()
        pmb.populate(pm)

        # Convert the module to a binding module
        llvm_module = llvm.parse_assembly(str(self.module))
        pm.run(llvm_module)

        print("After Optimization:\n", str(llvm_module))

        # Save the optimized module
        self.optimized_module = llvm_module

    def compile_and_execute(self):
        llvm.initialize()
        llvm.initialize_native_target()
        llvm.initialize_native_asmprinter()
        llvm_ir_parsed = self.optimized_module
        llvm_ir_parsed.verify()

        target_machine = llvm.Target.from_default_triple().create_target_machine()
        engine = llvm.create_mcjit_compiler(llvm_ir_parsed, target_machine)
        engine.finalize_object()

        entry = engine.get_function_address('main')
        cfunc = CFUNCTYPE(ctypes.c_int32)(entry)

        print(f"Function address: {entry}")
        print(f"Callable function: {cfunc}")

        result = cfunc()
        print(f"Output: {result}")

def compile_turing_machine(file_path):
    input_stream = FileStream(file_path)
    lexer = ExprLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = ExprParser(stream)
    tree = parser.machine()

    visitor = VisitorCompiler()
    
    visitor.visitMachine(tree)
    visitor.module.triple = llvm.get_default_triple()
    
    # Print the unoptimized module
    print("Unoptimized IR:\n", str(visitor.module))
    
    # Optimize the module
    visitor.optimize_module()
    
    # Compile and execute
    visitor.compile_and_execute()

# Usage
compile_turing_machine("input.tm")
