input: [1,1,0,0];
blank: " ";
start: right;
table:{
    right:{
        [1,0]: {R};
        " "  : {L: carry};
    };
    carry:{
        1      : {write: 0, L};
        [0," "]: {write: 1, L: done};
    };
}