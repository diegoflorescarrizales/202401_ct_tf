# Instalacion de librerias y generacion de achivos


pip install llvmlite antlr4-python3-runtime antlr4-tools graphviz

antlr4 -Dlanguage=Python3  Expr.g4

antlr4 -visitor -Dlanguage=Python3  Expr.g4 



# Generar codigo ir y compilarlo


    python main.py
    



Teoría de Compiladores 

Trabajo Final 

Sección: CC61 

Grupo: 3

Alumnos:

Datto Aponte, Antonio Francisco U202017033

Pastor salazar, Tomas Alonso U201916314

Flores Carrizales, Diego Armando U201922066


Docente: 

Carnaval Sanchez, Luis Martin

Introducción: problema, motivación, solución propuesta
Objetivos
Marco teórico
Metodologíá: el proceso paso a paso que siguió para desarrollar el proyecto.
Conclusiones
Referencias.

INTRODUCCIÓN 

1.1 Problema

La máquina de Turing es un modelo fundamental en la teoría de la computación que permite simular cualquier algoritmo. Sin embargo, la representación y ejecución de las máquinas de Turing puede ser compleja y requiere de un lenguaje formal que facilite su descripción y ejecución. En nuestro caso hemos hecho la implementación de la máquina de Turing usando el lenguaje de programación Python para describir sus componentes y transacciones de manera estructurada y clara. 

1.2 Motivación 

En la era actual de la informática, la enseñanza y comprensión de los fundamentos teóricos de la computación son cruciales para el avance del conocimiento y la tecnología. La máquina de Turing, como uno de los pilares de la teoría de la computación, ofrece una base esencial para entender cómo funcionan los algoritmos y las computadoras a un nivel fundamental.

1.3 Solución Propuesta

Para abordar los desafíos asociados con la creación y ejecución de máquinas de Turing, proponemos una solución integral basada en el desarrollo de un lenguaje de representación específico, acompañado de herramientas de simulación y visualización implementadas en Python. 

Las propuestas que tenemos son:
Genere un ejecutable para la máquina simulada e imprima el estado final de la cinta cuando esté terminado.
Habilite la ejecución Just-In-Time (JIT) para optimizar el proceso de simulación.
Permite una visualización de los gráficos mediante el lenguaje DOT de Graphviz.

OBJETIVOS

Desarrollar un lenguaje de representación para máquinas de Turing que sea claro, fácil de usar y extensible.
Implementar un intérprete en Python para este lenguaje que permita la simulación de la máquina.
Incorporar capacidades de ejecución JIT en el intérprete para mejorar la eficiencia y flexibilidad de la simulación.
Generar la representación gráfica de las máquinas de Turing en formato DOT para su visualización con Graphviz.
Facilitar el estudio y enseñanza de las máquinas de Turing mediante herramientas accesibles y bien documentadas.

MARCO TEÓRICO

Máquina de Turing:
La máquina de Turing, introducida por Alan Turing en 1936, es un modelo abstracto de computación que sirve como una representación idealizada de un ordenador. Se compone de:
Cinta infinita: Una serie de celdas que pueden contener símbolos de un alfabeto finito.
Cabezal de lectura/escritura: Se desplaza a lo largo de la cinta, leyendo y escribiendo símbolos.
Conjunto de estados: La máquina puede estar en uno de varios estados finitos.
Tabla de transiciones: Define las reglas para cambiar de estado, mover el cabezal y escribir en la cinta.

Ejecución Just-In-Time (JIT):
La ejecución JIT es una técnica utilizada para mejorar el rendimiento de los programas mediante la compilación dinámica de código durante la ejecución. Esta técnica permite optimizar las operaciones repetitivas y adaptar la ejecución a las condiciones específicas en tiempo real.

Lenguaje DOT y Graphviz:
El lenguaje DOT es un lenguaje de descripción de grafos utilizado por Graphviz, una herramienta de visualización de grafos. DOT permite describir nodos y aristas de un grafo de forma declarativa, lo que facilita la creación de representaciones visuales de estructuras complejas.


METODOLOGÍA

Para la generación del frontend creamos una gramática con ANTLR4 y python.
Creamos un visitor en python y llvmlite para generar el código ir, luego optimizarlo y finalmente compilarlo JIT.
En el mismo visitor se creó un generador del archivo dot que guardara la representación gráfica de la máquina de turing ingresada.

CONCLUSIONES

Se hizo una gramática con pyhton y antlr4 para leer un lenguaje de maquina de turing.
Se hizo un generador de código IR usando llvmlite y una clase visitor generada con nuestra gramatica.
Se hizo un generador de DOT usando graphviz que muestra la representación de la máquina ingresada.
Se optimizó y compilo el código IR generado JIT usando python.


Link del video: https://youtu.be/EcRaN3Xm9D0 

REFERENCIAS

Turing, A. M. (1936). On computable numbers, with an application to the Entscheidungsproblem. Proceedings of the London Mathematical Society, 42(2), 230-265. 
Graphviz - Graph Visualization Software. (n.d.). Retrieved from https://graphviz.org/ 
Sipser, M. (2006). Introduction to the Theory of Computation. Cengage Learning. 
Python Software Foundation. (n.d.). Python Programming Language. Retrieved from https://www.python.org/

