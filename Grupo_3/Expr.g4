grammar Expr;

INPUT : 'input' ;
BLANK : 'blank' ;
START : 'start' ;
TABLE : 'table' ;
DONE : 'done' ;
WRITE : 'write' ;
STRLIT: '"'([.]|' ')'"';
INT : [0-9]+ ;
WS: [ \t\n\r\f]+ -> skip ;
ID: [a-zA-Z_][a-zA-Z_0-9]* ;

int : INT;
strlit: STRLIT;
id: ID;

machine: inputstat blanckstat startstat tablestat;

inputstat: INPUT ':' vallist ';';

blanckstat: BLANK ':' strlit ';';

startstat: START ':' id ';';

tablestat: TABLE ':' '{' defstate* '}';

defstate: id ':' '{' defrule* '}' ';' ;

defrule: ((int | strlit) | vallist) ':' defactions ';';

defactions: '{' defaction  (',' defaction)* '}' ;

defaction: (defmove | defwrite);

defmove
    : ('L'|'R') 
    | ('L'|'R') ':' (id|'done')
;

defwrite: WRITE ':' int;

vallist: '[' (int | strlit) (',' (int | strlit))*  ']';